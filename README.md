# Parquet_API
An flask-pyspark app used to append CSV files to parquet tables in HDFS

## Usage

### Local Development
```
$ git clone...
$ cd <>
```

### Setting up virtual environment
In the top level of parquet_api, run:

```
$ virtualenv -p python3 venv
$ source ./venv/bin/activate # to activate virtual environment(source ./venv/bin/activate.fish if using fish command line)
$ pip install -r requirements.txt
```

#### Make sure to activate the virtual environment for all the following steps. To deactivate, simply run `deactivate`.

### Server
In the top level of parquet_api, run:

```
$ gunicorn app:app
```

### How to use
after run the above command, go to http://127.0.0.1:8000/  
For the two paths you need to enter:      
Left: csv path(e.g:  ./test_data/test_data.csv).  
Right: parquet path(e.g:  test.parquet).


## Resources

1. [static files in flask](https://stackoverflow.com/questions/20646822/how-to-serve-static-files-in-flask)

2. [python3 virtualenv](https://stackoverflow.com/questions/23842713/using-python-3-in-virtualenv)
