from flask import Flask, render_template, url_for, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from pyspark import SparkConf, SparkContext
from pyspark.sql.types import *
import sys,os 
from pyspark.sql.session import SparkSession

lowQualVarsSchema = StructType([StructField('accession', StringType(), False),
                                StructField('library_type', StringType(), False),
                                StructField('library_id', StringType(), False),
                                StructField('library_lanes', StringType(), False),
                                StructField('ngs_sequencer_id', StringType(), False),
                                StructField('ngs_run', StringType(), False),
                                StructField('pipeline_version', StringType(), False),
                                StructField('plm_txs_id', StringType(), False),
                                StructField('vcf_chrom', StringType(), False),
                                StructField('vcf_pos', LongType(), False),
                                StructField('vcf_ref', StringType(), False),
                                StructField('vcf_alt', StringType(), False),
                                StructField('obs_start_pos', LongType(), False),
                                StructField('obs_end_pos', LongType(), False),
                                StructField('obs_ref', StringType(), True),
                                StructField('obs_alt', StringType(), True),
                                StructField('vcf_qual', IntegerType(), False),
                                StructField('vcf_filter', StringType(), False),
                                StructField('vcf_info', StringType(), False),
                                StructField('vcf_format', StringType(), False),
                                StructField('vcf_sample', StringType(), False),
                                StructField('alt_percent', DoubleType(), False),
                                StructField('coverage', IntegerType(), False),
                                StructField('genotype_qual', IntegerType(), False),
                                StructField('uploaded', TimestampType(), False)])

CoverageSchema = StructType([StructField('accession', StringType(), False),
                                StructField('assembly', StringType(), False),
                                StructField('gender', StringType(), False),
                                StructField('subtype', StringType(), False),
                                StructField('chrom', StringType(), False),
                                StructField('start_pos', LongType(), False),
                                StructField('end_pos', LongType(), False),
                                StructField('gene', StringType(), True),
                                StructField('name', StringType(), True),
                                StructField('capture', IntegerType(), False),
                                StructField('library_id', StringType(), False),
                                StructField('platform', StringType(), False),
                                StructField('mean_cov', StringType(), False),
                                StructField('normalized_cov', StringType(), False),
                                StructField('lowcoverage_regions', DoubleType(), False),
                                StructField('lowcoverage_positions', IntegerType(), False),
                                StructField('depth_threshold', IntegerType(), False),
                                StructField('time_stamp', TimestampType(), False)])

coverage_header = [
    "accession", "assembly", "gender", "subtype", "chrom", "start_pos", "end_pos", "gene", "name", "capture", "library_id",
    "platform", "mean_cov", "normalized_cov", "lowcoverage_regions", "lowcoverage_positions", "depth_threshold", "time_stamp"
]

lowqualvar_header= [
    "accession", "library_type", "library_id", "library_lanes", "ngs_sequencer_id", "ngs_run", "pipeline_version", "plm_txs_id", "vcf_chrom", "vcf_pos", "vcf_ref",
    "vcf_alt", "obs_start_pos", "obs_end_pos", "obs_ref", "obs_alt", "vcf_qual", "vcf_filter","vcf_info","vcf_format","vcf_sample","alt_percent","coverage","genotype_qual","uploaded"]


app = Flask(__name__)
#to remove warning message
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
#connect to SQL database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'

db = SQLAlchemy(app)

class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    csv_path = db.Column(db.String(200), nullable=False)
    parquet_path = db.Column(db.String(200), nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Todo %r>' % self.id

def check_schema(column_header):
    if column_header == lowqualvar_header:
        schema = lowQualVarsSchema
        print(schema)
    elif column_header == coverage_header:
        schema = CoverageSchema
        print(schema)
    else:
        schema = None
    return schema

def get_parquet_schema(hive_client, HIVE_TABLE):
    """ Fetch Parquet table and return list of columns """
    hive_table = hive_client.read.parquet(HIVE_TABLE)
    schema = hive_table.schema
    return [i.name for i in schema.fields]


def get_csv_header(spark_client, HDFS_FILE):
    """ Fetch header from CSV file and return list of columns """
    csv_file = spark_client.textFile(HDFS_FILE)
    return csv_file.first().split(',')


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        csv = request.form['csv_path']
        parquet = request.form['parquet_path']

        #set the default path of parquet table to 'LowQualVar.parquet'
        default_parquet = 'LowQualVar.parquet'
        
        #when parquet path is left empty, set default parquet path to lowqualvar.parquet.
        if parquet == "":
            parquet = default_parquet
        
        #CSV path is required
        if csv == "":
            
            return 'CSV file is empty, Please specify upload csv file(s) path and parquetTable!'

        #create the database entry
        Append = Todo(csv_path= csv,parquet_path= parquet)

        

        #TO DO: 
        #CHECK CSV FILE validation
        #COMPARE CSV header to parquet table

        #creat spark session
        conf = (SparkConf().setAppName("archiveLowQualVariants2Parquet"))
        sc = SparkContext.getOrCreate(conf=conf)
        spark = SparkSession(sc)
        
        csv_columns = get_csv_header(sc, csv)
        parquet_schema= get_parquet_schema(spark,parquet)
 
        if csv_columns == parquet_schema:
            #logMsg("Uploading content from " + uploadPath + " to " + parquetPath)
            schema = check_schema(csv_columns)
  
            varsDf = spark.read.load(csv, format="csv", delimiter=",", header = True, schema=schema)

            varsDf.coalesce(1).write.mode("append").parquet(parquet)
            print("Uploading content from " + csv+ " to " + parquet)
            try:
                #record finished appending in the database
                db.session.add(Append)
                db.session.commit()
                return redirect('/')
            except:
                return 'There was an issue adding your task'
        
        else:
            return 'Column names does not match'

    else:
        #record the date 
        tasks = Todo.query.order_by(Todo.date_created).all()
        return render_template('index.html', tasks=tasks)



@app.route('/delete/<int:id>')
def delete(id):
    task_to_delete = Todo.query.get_or_404(id)

    try:
        db.session.delete(task_to_delete)
        db.session.commit()
        return redirect('/')
    except:
        return 'There was a problem deleting that task'

@app.route('/update/<int:id>', methods=['GET', 'POST'])
def update(id):
    task = Todo.query.get_or_404(id)

    if request.method == 'POST':
        task.content = request.form['content']

        try:
            db.session.commit()
            return redirect('/')
        except:
            return 'There was an issue updating your task'

    else:
        return render_template('update.html', task=task)


if __name__ == "__main__":
    app.run(debug=True)
